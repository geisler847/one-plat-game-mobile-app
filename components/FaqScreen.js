import React from 'react';
import { ScrollView, View, Text, StyleSheet, StatusBar, ActivityIndicator } from 'react-native';
import { List,Button } from 'react-native-paper';
import * as Font from 'expo-font';
import Faqs from '../assets/faqs.json';


export default class FaqScreen extends React.Component {

  constructor(){
    super();
    this.scrollView = React.createRef();
    this.scrollToPosition = this.scrollToPosition.bind(this);
    this.state = {
      fontLoaded: false,
      dropdown1y: null,
      dropdown2y: null,
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    const faqs = await this.loadFaqs();

    this.setState({
      fontLoaded: true,
      spielablaufFaqs: faqs.spielablaufFaqs,
      spielkartenFaqs: faqs.spielkartenFaqs
    });
  }

  scrollToPosition(pos) {
    this.scrollView.current.scrollTo({
      y: pos, animated:true
    });
  }

  async loadFaqs() {
    const faqs = {
      spielablaufFaqs: [],
      spielkartenFaqs: []
    }

    Faqs.spielablauf.forEach(faq => {
      faqs.spielablaufFaqs.push(
        <Text key={faq.frage} style={styles.question}>
          {faq.frage}
        </Text>,
        <Text key={faq.antwort} style={styles.answer}>
        {faq.antwort}
      </Text>
      )
    });

    Faqs.spielkarten.forEach(faq => {
      faqs.spielkartenFaqs.push(
        <Text key={faq.frage} style={styles.question}>
          {faq.frage}
        </Text>,
        <Text key={faq.antwort} style={styles.answer}>
        {faq.antwort}
      </Text>
      )
    });

    return faqs;
  }

    render() {
      if (this.state.fontLoaded) {

      return (
        <ScrollView style={styles.scrollView} ref={this.scrollView}>

          <View style={styles.titleView}>
            <View>
              <View style={styles.horizontalLine} />
              <Text style={styles.titleText}>Du hast eine Frage zu One Planet Game?</Text>
              <Text style={styles.titleText}>Hier ist die Antwort!</Text>
              <View style={styles.horizontalLine} />
            </View>
            <Text style={styles.baseText}>
              Hier findest du klare Antworten auf die Fragen, die häufig zu One Planet Game gestellt
              werden. Es gibt Fragen zum <Text style={styles.bold}>Spielablauf</Text> und 
              allgemeine Fragen zu den <Text style={styles.bold}>Spielkarten</Text>.
            </Text>
          </View>
          
          <View onLayout={event => {
            const layout = event.nativeEvent.layout;
            this.setState({
              dropdown1y: layout.y
            })
          }}>
            <List.Section style={styles.listSections}>
              <List.Accordion
                title="Fragen zum Spielablauf"
                titleStyle={styles.normalText}
                theme={{ colors: { primary: 'black' } }}
                style={styles.accordion}
                onPress={() => {
                  setTimeout(_ => {
                    this.scrollToPosition(this.state.dropdown1y);
                  }, 0);
                }}>
                { this.state.spielablaufFaqs }
                <Button onPress={() => {
                  setTimeout(_ => {
                    this.scrollToPosition(this.state.dropdown1y);
                  }, 0);
                }}>Nach oben</Button>
              </List.Accordion>
            </List.Section>
            </View>

            <View onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.setState({
                dropdown2y: layout.y
              })
            }}>
            <List.Section style={styles.listSections}>
              <List.Accordion
                title="Fragen zu den Spielkarten"
                theme={{ colors: { primary: 'black' }}}
                style={styles.accordion}
                onPress={() => {
                  setTimeout(_ => {
                    this.scrollToPosition(this.state.dropdown2y);
                  }, 0);
                }}
              >
                { this.state.spielkartenFaqs }
                <Button onPress={() => {
                  setTimeout(_ => {
                    this.scrollToPosition(this.state.dropdown2y);
                  }, 0);
                }}>Nach oben</Button>
              </List.Accordion>
            </List.Section>
          </View>

        </ScrollView>
      );
    } else {
      return(<ActivityIndicator size="small"/>)
    }
    }
  }

  const styles = StyleSheet.create({
    scrollView: {
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 10,
      marginTop: StatusBar.currentHeight,
      backgroundColor: "#ffffff"
    },
    titleView: {
      padding: 15,
      paddingTop: 30,
      width: "100%",
      height: "100%",
      flex: 1,
      flexDirection:"column",
      justifyContent: 'center',
      alignItems: 'flex-start' 
    },
    horizontalLine: {
      borderBottomColor: 'black',
      borderBottomWidth: 2,
      margin: 5
    },
    titleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 20,
    },
    baseText: {
      fontFamily: 'Lato-Regular',
      fontSize: 15,
      paddingTop: 10
    },
    bold: {
      fontWeight: 'bold'
    },
    normalText: {
      fontWeight: 'normal'
    },
    listSections: {
      backgroundColor: '#ebebeb',
      marginBottom: 20
    },
    accordion: {
      padding: 10,
      backgroundColor: '#acce37'
    },
    question: {
      paddingLeft: 15,
      padding: 10,
      fontSize: 15,
      fontFamily: 'Lato-Bold'
    },
    answer: {
      paddingLeft: 15,
      padding: 10,
      fontSize: 13,
      fontFamily: 'Lato-Regular'
    }
  });