import React from 'react';
import { View, Text, StyleSheet, StatusBar, ActivityIndicator, ScrollView, Linking,
TouchableOpacity } from 'react-native';
import { Button } from 'react-native-paper';
import * as Font from 'expo-font';

export default class FootprintScreen extends React.Component {

  constructor(){
    super();
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }

    render() {
      if (this.state.fontLoaded) {
      return (
        <ScrollView style={styles.scrollView}>
          <View style={styles.wrappingView}>
            <View>
              <View style={styles.horizontalLine} />
              <Text style={styles.titleText}>Online-Fußabdruckrechner</Text>
              <View style={styles.horizontalLine} />
            </View>

            <Text style={styles.baseText}>Was ist dein ökologischer Fußabdruck?
            Wie viele Planeten brauchen wir, wenn jeder so lebt wie du?
            </Text>

            <Text style={styles.baseText}>Unabhängig davon, ob du One Planet Game zur Thematisierung des ökologischen
             Fußadbrucks im Unterricht verwendest, empfehlen wir auch einen 
             konventionellen Online-Fußabdruckrechner auszuprobieren. Hier unsere Empfehlung:
            </Text>
            <Text style={styles.calculatorText}>
            Das <Text style={styles.bold}>Global Footprint Network</Text>, das den ökologischen Fußabdruck methodisch betreut, hat einen eigenen,
            sehr guten Fußabdruckrechner entworfen (externe URL):
            </Text>
            <TouchableOpacity style={styles.buttonWrappers} onPress={() => Linking.openURL('https://www.footprintcalculator.org')}>
              <Button icon="arrow-right" style={styles.button} mode="contained" color='#acce37'>
                <Text style={styles.baseText}>Footprintcalculator</Text>
              </Button>
            </TouchableOpacity>
            <Text style={styles.calculatorText}>
            Ein weiterer empfehlenswerter Fußabdruckrechner wurde
            von <Text style={styles.bold}>Brot für die Welt</Text> entworfen und ist
            hier auffindbar (externe URL):
            </Text>
            <TouchableOpacity style={styles.buttonWrappers} onPress={() => Linking.openURL('https://www.fussabdruck.de/fussabdrucktest')}>
              <Button icon="arrow-right" style={styles.button} mode="contained" color='#acce37'>
                <Text style={styles.baseText}>Fußabdruckrechner</Text>
              </Button>
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    } else {
      return(<ActivityIndicator size="small"/>)
      }
    }
  }


  const styles = StyleSheet.create({
    scrollView: {
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 10,
      marginTop: StatusBar.currentHeight,
      backgroundColor: "#ffffff"
    },
    wrappingView: {
      padding: 15,
      paddingTop: 30,
      width: "100%",
      height: "100%",
      flex: 1,
      flexDirection:"column",
      alignItems: 'center',
      justifyContent: 'flex-start' 
    },
    horizontalLine: {
      borderBottomColor: 'black',
      borderBottomWidth: 2,
      margin: 5
    },
    titleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 20
    },
    baseText: {
      fontFamily: 'Lato-Regular',
      fontSize: 15,
      paddingTop: 10,
      marginBottom: 10
    },
    bold: {
      fontWeight: 'bold'
    },
    buttonWrappers: {
      width: '100%',
      height: '10%'
    },
    button: {
      margin: 10
    },
    calculatorText: {
      marginTop: 10
    }
  });