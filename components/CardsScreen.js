import React from 'react';
import { View, Text, ActivityIndicator, ScrollView, StyleSheet,
TouchableOpacity } from 'react-native';
import { Button } from 'react-native-paper';
import * as Font from 'expo-font';

export default class CardsScreen extends React.Component {

  constructor(){
    super();
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#acce37'
    }
  }

    render() {
      if (this.state.fontLoaded) {
      return (
        <ScrollView contentContainerStyle={styles.scrollView}>
          <View style={styles.wrappingView}>
            <View style={styles.wrappingCategoryView}>

            <View style={styles.categoryRow}>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Energie')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Energie</Text>
                  </Button>
                </TouchableOpacity>
              </View>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Freizeit')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Freizeit</Text>
                  </Button>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.categoryRow}>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Ernährung')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Ernährung</Text>
                  </Button>
                </TouchableOpacity>
              </View>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Kleidung')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Kleidung</Text>
                  </Button>
                </TouchableOpacity>
              </View>
            </View>
            
            <View style={styles.categoryRow}>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Konsum')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Konsum</Text>
                  </Button>
                </TouchableOpacity>
              </View>
              <View style={styles.buttonView}>
                <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Haushalt')}>
                  <Button style={styles.categoryButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Haushalt</Text>
                  </Button>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.categoryRow}>
              <View style={styles.lastButtonView}>
                <TouchableOpacity style={styles.lastButtonWrapper} onPress={() => this.props.navigation.navigate('Transport')}>
                  <Button style={styles.lastButton} mode="contained" color='#acce37'>
                    <Text style={styles.buttonText}>Transport</Text>
                  </Button>
                </TouchableOpacity>
              </View>
            </View>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return(<ActivityIndicator size="small"/>)
      }
    }
  }

  const styles = StyleSheet.create({
    scrollView: {
      height: '100%',
      backgroundColor: "#ffffff",
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: "center",
    },
    titleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 20,
    },
    wrappingView: {
      padding: 10,
      width: "100%",
      height: "100%",
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: "center",
      backgroundColor: "#ffffff"
    },
    wrappingCategoryView: {
      padding: 10,
      width: "100%",
      height: "100%",
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: "stretch"
    },
    categoryRow: {
      padding: 5,
      width: "100%",
      height: '20%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: "stretch"
    },
    buttonText: {
      fontFamily: 'Lato-Bold'
    },
    buttonView: {
      width: '45%',
      height: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: "stretch"
    },
    lastButtonView: {
      width: '100%',
      height: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: "stretch"
    },
    buttonWrappers: {
      width: '100%',
    },
    categoryButton: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: "center"
    },
    lastButtonWrapper: {
      width: '50%',
    },
    lastButton: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: "center"
    }
  });