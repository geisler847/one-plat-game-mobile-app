import React from 'react';
import { ScrollView, View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { List, Button } from 'react-native-paper';
import * as Font from 'expo-font';
import Cards from '../../assets/cards.json';


export default class ConsumeCardsScreen extends React.Component {

  constructor(){
    super();
    this.scrollView = React.createRef();
    this.scrollToPosition = this.scrollToPosition.bind(this);
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../../assets/fonts/Lato-Thin.ttf'),
    });

    const listSections = await this.fetchCards();

    this.setState({
      fontLoaded: true,
      listSections: listSections
    });
  }

  scrollToPosition(pos) {
    this.scrollView.current.scrollTo({
      y: pos, animated:true
    });
  }

  async fetchCards() {
    const listSections = [];

    Cards.forEach(card => {
      if (card.Kategorie == 'Konsum') {
        let yCoordinate = 0;        

        listSections.push(
          <View onLayout={event => {
              const layout = event.nativeEvent.layout;
              yCoordinate = layout.y;
            }}>
            <List.Section key={card.Kartenkennziffer} style={styles.listSections}>
              <List.Accordion
                title={card.Kurzbeschreibung}
                titleStyle={{fontWeight: 'bold'}}
                theme={{ colors: { primary: 'black' }}}
                style={styles.accordion}
                onPress={() => {              
                  setTimeout(_ => {
                    this.scrollToPosition(yCoordinate);
                  }, 0);
                }}>
                <View style={styles.titleView}>
                  <Text style={styles.titleText}>{card.Kurzbeschreibung}</Text>
                </View>
                <Text style={styles.question}>
                  Typ
                </Text>
                <Text style={styles.answer}>
                  {card.Typ ? card.Typ : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  Gegenstandsmenge
                </Text>
                <Text style={styles.answer}>
                  {card.GegenstandsMenge ? card.GegenstandsMenge : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  Erklärung
                </Text>
                <Text style={styles.answer}>
                  {card.Erklaerung ? card.Erklaerung : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  Annahmen
                </Text>
                <Text style={styles.answer}>
                  {card.Annahmen ? card.Annahmen : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  Berechnung
                </Text>
                <Text style={styles.answer}>
                  {card.Berechnung ? card.Berechnung : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  CO2-kg-Menge
                </Text>
                <Text style={styles.answer}>
                  {card.Co2kgMenge ? card.Co2kgMenge : 'keine Angabe'}
                </Text>
                <Text style={styles.question}>
                  Quelle
                </Text>
                <Text style={styles.answer}>
                  {card.Quelle ? card.Quelle : 'keine Angabe'}
                </Text>
                <Button onPress={() => {
                  setTimeout(() => {
                    this.scrollToPosition(yCoordinate);
                  });
                }}>Nach oben</Button>
              </List.Accordion>
            </List.Section>
          </View>
        );
      }
    });

    return listSections;
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#acce37'
    }
  }

    render() {
      if (this.state.fontLoaded) {

      return (
        <ScrollView style={styles.scrollView} ref={this.scrollView}>

          <View style={styles.wrappingView}>
            <View>
              <View style={styles.horizontalLine} />
              <Text style={styles.titleText}>Hier findest du alle Karten, die zur Kategorie 'Konsum' gehören.</Text>
              <View style={styles.horizontalLine} />
            </View>
            <Text style={styles.baseText}>
              Im folgenden sind alle Namen der Konsum-Karten aufgelistet. Klicke auf einen Namen, um genauere Informationen
              der Karte zu erhalten.
            </Text>
          </View>
          
          { this.state.listSections }

        </ScrollView>
      );
    } else {
      return(<ActivityIndicator size="small"/>)
    }
    }
  }

  const styles = StyleSheet.create({
    scrollView: {
      padding: 10,
      backgroundColor: "#ffffff"
    },
    wrappingView: {
      padding: 15,
      paddingTop: 20,
      width: "100%",
      height: "100%",
      flex: 1,
      flexDirection:"column",
      justifyContent: 'center',
      alignItems: 'flex-start'
    },
    horizontalLine: {
      borderBottomColor: 'black',
      borderBottomWidth: 2,
      margin: 5
    },
    titleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 20,
    },
    baseText: {
      fontFamily: 'Lato-Regular',
      fontSize: 15,
      paddingTop: 10
    },
    accordion: {
      padding: 10,
      backgroundColor: '#acce37'
    },
    listSections: {
      backgroundColor: '#ebebeb',
      marginBottom: 20
    },
    titleView: {
      flexDirection:"column",
      alignItems: 'center',
      justifyContent: 'center',
      padding: 15
    },
    cardTitleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 18,
      textDecorationLine: 'underline'
    },
    question: {
      paddingLeft: 15,
      padding: 10,
      fontSize: 15,
      fontFamily: 'Lato-Bold'
    },
    answer: {
      paddingLeft: 15,
      padding: 10,
      fontSize: 13,
      fontFamily: 'Lato-Regular'
    }
  });