import React from 'react';
import { View, Text, StyleSheet, StatusBar, ActivityIndicator, ScrollView, Image, Linking,
   SafeAreaView, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';
import * as Font from 'expo-font';
import Lightbox from 'react-native-lightbox-v2';
import { Button } from 'react-native-paper';

export default class AboutScreen extends React.Component {

  constructor(){
    super();
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }

  render() {
    if (this.state.fontLoaded) {
    return (
        <ScrollView style={styles.scrollView}>
          <SafeAreaView style={styles.wrappingView}>
            <View>
              <View style={styles.horizontalLine} />
              <Text style={styles.titleText}>Wie viele Planeten bräuchte es,
              wenn alle so leben würden, wie wir?</Text>
              <View style={styles.horizontalLine} />
            </View>
            <Text style={styles.baseText}>Diese Frage ist Ausgangspunkt unseres Projektes. 
            Es ist eine Frage, die wir uns als Gesellschaft stellen müssen, aber auch als Individuen – 
            und in unserem alltäglichen Leben. Um eine nachhaltige Zukunft zu ermöglichen, braucht es nicht
            nur technologischen Fortschritt, sondern vor allem einen Wandel unserer Wirtschafts- und Lebensweise.
            </Text>
            <Text style={styles.baseText}>One Planet Game ist ein Projekt der Studierendeninitiative enactus
            Münster, in der Studierende zusammenkommen, um Unternehmertum nicht nur neu zu denken, sondern 
            auch zu erproben. Dazu gründen sie soziale und ökologische Projekte mit unternehmerischem Ansatz,
              um die Welt im Kleinen zu verbessern. Das Projektteam von One Planet Game verbindet die Überzeugung,
            dass Bildung alleine vielleicht nicht die Welt retten wird, aber dennoch ein unverzichtbarer
              Bestandteil von gesellschaftlichem Wandel ist.
            </Text>
            <View style={styles.imageView}>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/teamss19.jpg')
                  }
                  style={styles.horizontalImage}
                />
              </Lightbox>
              <Text style={styles.imageText}> Das Team im Sommersemester 2019 (Klicke zum Vergrößern)</Text>
            </View>
            <Text style={styles.subTitleText}>Unsere Idee</Text>
            <Text style={styles.baseText}>Wir möchten, dass Jugendliche ein konkretes Ressourcenbewusstsein
            für ihren persönlichen Lebensstil entwickeln und in die Lage versetzt werden, ihr Umweltwissen
            in Alltagssituationen anzuwenden. Zwar haben wir manchmal schon ein grundsätzliches Gespür
            für die Umweltfreundlichkeit von Alltags-Aktivitäten, doch ist dieses selten viel differenzierter als:
            „das ist gut“ und „das ist schlecht“.
            </Text>
            <Text style={styles.baseText}>Wir benötigen Konzepte wie den ökologischen Fußabdruck, um wirklich
            faktisch zu beurteilen, welche Auswirkungen unsere Handlungen haben. Und schließlich müssen wir
            die Denkmuster trainieren, die wir brauchen, wenn wir im Supermarkt stehen und uns fragen, welche
            Entscheidung die richtige ist. Dazu gehören Fragen wie: Wo kommt’s her? Was steckt drin? Wie aufwendig
            ist die Produktion?
            </Text>
            <Text style={styles.baseText}>Wir haben uns mit diesen Fragen detailliert auseinandergesetzt und mit 
            großer Sorgfalt auf Basis von öffentlich zugänglichen Studien und Umweltdatenbanken den ökologischen
            Fußabdruck verschiedenster Güter und Dienstleistungen berechnet. Unsere Erkenntnisse haben wir in 
            ein verständliches, unterhaltsames und ansprechend designtes Kartenspiel verwandelt, mit dem wir der
            Bildung für nachhaltige Entwicklung ein zugängliches Unterrichtskonzept an die Hand geben möchten.
            </Text>
            <View style={styles.ytVideo}>
              <WebView
                style={styles.webViewStyle}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{uri: 'https://www.youtube.com/embed/3p8N0PxRBtc' }}
              />  
            </View>
            <Text style={styles.subTitleText}>Das Projekt</Text>
            <Text style={styles.baseText}>One Planet Game wurde an verschiedenen Schulen mit ingesamt 200 
            Schülerinnen und Schülern sehr erfolgreich getestet. Seit April 2018 ist unser Spiel als Produkt
            erhältlich und wird inzwischen von mehr als 60 Schulen eingesetzt. Unser Projekt ist nicht
            gewinnorientiert und wir als freiwillig Engagierte haben keinerlei finanzielle Vorteile durch
            die Projektarbeit. Unser Lohn sind stattdessen wertvolle Erfahrungen, Gemeinschaft, Erfolgserlebnisse 
            und die Zuversicht, einen positiven gesellschaftlichen Beitrag zu leisten. Wir entwickeln unser
            Bildungskonzept auch in Zukunft weiter und möchten damit noch mehr Schülerinnen und Schüler in 
            Deutschland erreichen.
            </Text>
            <View style={styles.twoImages}>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/projekt1.jpg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/projekt2.jpeg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
            </View>
            <View style={styles.twoImages}>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/verpackungen.jpeg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/projekt3.jpeg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
            </View>
            <View style={styles.twoImages}>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/projekt4.jpeg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/projekt5.jpg')
                  }
                  style={styles.smallImage}
                />
              </Lightbox>
            </View>
            <View style={styles.twoImages}>
              <View style={styles.imageView}>
                <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                  <Image
                    source={
                      require('../assets/images/teamws1718.jpg')
                    }
                    style={styles.smallImage}
                  />
                </Lightbox>
                <Text style={styles.imageText}> WS 2017/18 </Text>
                <Text style={styles.imageText}> (Klicke zum Vergrößern) </Text>
              </View>
              <View style={styles.imageView}>
                <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                  <Image
                    source={
                      require('../assets/images/teamws1819.jpg')
                    }
                    style={styles.smallImage}
                  />
                </Lightbox>
                <Text style={styles.imageText}> WS 2018/19 </Text>
                <Text style={styles.imageText}> (Klicke zum Vergrößern) </Text>
              </View>
            </View>
            <View style={styles.imageView}>
              <Lightbox activeProps={{style: {resizeMode: 'contain', width: null}}}>
                <Image
                  source={
                    require('../assets/images/teamss192.jpg')
                  }
                  style={styles.horizontalImage}
                />
              </Lightbox>
              <Text style={styles.imageText}> Das Team im Sommersemester 2019 (Klicke zum Vergrößern)</Text>
            </View>
            <View style={styles.privacy}>
              <TouchableOpacity style={styles.buttonWrappers} onPress={() => Linking.openURL('https://oneplanetgame.org/unsere-agb/')}>
                <Button icon="arrow-right" style={styles.button} mode="contained" color='#FFF'>
                  <Text style={styles.baseText}>AGB</Text>
                </Button>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonWrappers} onPress={() => Linking.openURL('https://oneplanetgame.org/datenschutz/')}>
                <Button icon="arrow-right" style={styles.button} mode="contained" color='#FFF'>
                  <Text style={styles.baseText}>Datenschutz</Text>
                </Button>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </ScrollView>
    );
  } else {
    return(<ActivityIndicator size="small"/>)
  }
  }
}

const styles = StyleSheet.create({
  scrollView: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginTop: StatusBar.currentHeight,
    backgroundColor: "#ffffff"
  },
  titleText: {
    fontFamily: 'Lato-Bold',
    fontSize: 20,
  },
  subTitleText: {
    fontFamily: 'Lato-Bold',
    fontSize: 18,
    marginTop: 20
  },
  baseText: {
    fontFamily: 'Lato-Regular',
    fontSize: 15,
    paddingTop: 10,
    marginBottom: 0
  },
  wrappingView: {
    padding: 15,
    paddingTop: 30,
    width: "100%",
    height: "100%",
    flex: 1,
    flexDirection:"column",
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  horizontalLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    margin: 5
  },
  imageView: { 
    padding: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  twoImages: {
    marginTop: 10,
    padding: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  horizontalImage: {
    resizeMode: 'contain',
    width: 280,
    height: 180,
  },
  smallImage: {
    resizeMode: 'contain',
    width: 140,
    height: 140,
  },
  imageText: {
    fontFamily: 'Lato-Italic',
    fontSize: 12,
  },
  webViewStyle: {
    margin: 10,
    opacity: 0.99
  },
  ytVideo: {
    width: '100%', 
    height: 230
  },
  privacy: {
    width: '100%',
    padding: 20,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  buttonWrappers: {
    width: '100%',
  },
  button: {
    margin: 10
  },
});