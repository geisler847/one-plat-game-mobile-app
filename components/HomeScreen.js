import React from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, Linking,
   TouchableOpacity, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-paper';
import * as Font from 'expo-font';


export default class HomeScreen extends React.Component {

  constructor(){
    super();
    this.state = {
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }

  static navigationOptions = {
      headerShown: false
  }
    

  render() {
    if (this.state.fontLoaded) {
    return (
    <View style={styles.wrappingView}>
      <View>
        <View style={styles.horizontalLine} />
          <View style={styles.imageView}>
            <Image
              source={
                  require('../assets/images/opg.png')
              }
              style={styles.welcomeImage}
            />
          </View>
          <View style={styles.horizontalLine} />
      </View>
      {/* <Button mode="contained" color='#acce37' onPress={() => this.props.navigation.navigate('Quiz')}>
        <Text style={styles.buttonText}>Quiz (tbd)</Text>
      </Button> */}
      
      <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Spielanleitung')}>
        <Button mode="contained" style={styles.buttons} color='#acce37'>
          <Text style={styles.buttonText}>Spielanleitung</Text>
        </Button>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonWrappers} onPress={() => this.props.navigation.navigate('Kategorien')}>
        <Button mode="contained" style={styles.buttons} color='#acce37'>
          <Text style={styles.buttonText}>Die Karten</Text>
        </Button>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonWrappers} onPress={() => Linking.openURL('https://oneplanetgame.org/bestellen/')}>
        <Button icon="arrow-right" style={styles.lowestButton} mode="contained" color='#acce37'>
          <Text style={styles.buttonText}>Zum Shop</Text>
        </Button>
      </TouchableOpacity>

    </View>
    );
  } else {
    return(<ActivityIndicator size="small"/>)
    }
  }
}

const styles = StyleSheet.create({
  welcomeImage: {
      width: '50%',
      height: 100,
      resizeMode: 'contain',
      marginTop: 3,
      marginLeft: -10,
      alignContent: "center"
    },
  wrappingView: {
    padding: 40,
    width: "100%",
    height: "100%",
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: "stretch",
    backgroundColor: "#ffffff"
  },
  horizontalLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 2,
  },
  imageView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5
  },
  buttonWrappers: {
    width: '100%',
    height: '10%'
  },
  buttons: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: "center",
  },
  lowestButton: {
    marginBottom: 30,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: "center"
  },
  buttonText: {
    fontFamily: 'Lato-Bold',
    padding: 100
  }
})