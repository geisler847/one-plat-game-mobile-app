import React from 'react';
import { View, Image, ScrollView, StyleSheet, ActivityIndicator,
TouchableOpacity, Text } from 'react-native';
import { Button, Card, Title } from 'react-native-paper';
import * as Font from 'expo-font';
import Lightbox from 'react-native-lightbox-v2';
import { WebView } from 'react-native-webview';


export default class GameManualScreen extends React.Component {

  constructor(){
    super();
    this.scrollView = React.createRef();
    this.scrollToPosition = this.scrollToPosition.bind(this);
    this.state = {
      card1y: null,
      card1disable: false,
      card2y: null,
      card2: false,
      card2disable: false,
      card3y: null,
      card3: false,
      card3disable: false,
      card4: false,
      card4disable: false,
      fontLoaded: false
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'Lato-Black': require('../assets/fonts/Lato-Black.ttf'),
      'Lato-Bold': require('../assets/fonts/Lato-Bold.ttf'),
      'Lato-Italic': require('../assets/fonts/Lato-Italic.ttf'),
      'Lato-Regular': require('../assets/fonts/Lato-Regular.ttf'),
      'Lato-Thin': require('../assets/fonts/Lato-Thin.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }

  scrollToPosition(pos) {
    this.scrollView.current.scrollTo({
      y: pos, animated:true
    });
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#acce37'
    }
  }

  render() {
    if (this.state.fontLoaded) {
      
    return (
      <ScrollView style={styles.scrollView} ref={this.scrollView}>
        <View style={styles.wrappingView}>

          <View style={styles.introductionView}>
            <View>
              <View style={styles.horizontalLine} />
              <Text style={styles.titleText}>Wie spielt man One Planet Game?</Text>
              <View style={styles.horizontalLine} />
            </View>
            <Text style={styles.baseText}>
              <Text style={{ fontWeight: 'bold' }}>
                Vorbereitung:
              </Text>
            </Text>
            <Text style={styles.baseText}>
              Bildet Zweier-Teams.
              Jedes Team bekommt 5 Handkarten, die restlichen Karten kommen als Nachziehstapel
              in die Mitte. Ihr dürft euch die Fußabdruck-Punkte auf euren eigenen Karten ansehen, aber passt auf, dass die Anderen sie nicht sehen können!
            </Text>
            <Text style={styles.baseText}>
              <Text style={{ fontWeight: 'bold' }}>
                  Spielablauf:
              </Text>
            </Text>
            <Text style={styles.baseText}>
              Das Team mit den größten
              Füßen fängt an. Es legt eine Karte mit der Vorderseite nach oben auf den Tisch und liest den Text darauf laut vor. Dann geht es im Uhrzeigersinn weiter.
              Die Punkte auf den gelegten Karten dürfen zusammen nicht mehr als 20 ergeben, sonst ist euer Fußabdruck größer als „ein Planet“.
            </Text>
            <Text style={styles.baseText}>
              Wenn ihr an der Reihe seid, müsst ihr euch fragen, ob die 20 Punkte schon überschritten wurden oder nicht.
              Wenn ihr „NEIN“ denkt, dann legt ihr eine weitere Karte ab.
              Wenn ihr „JA“ denkt, dann legt ihr keine Karte, sondern klopft auf den Tisch und alle Karten in der Mitte werden aufgedeckt, zusammengezählt und unter den Stapel gelegt.
              Sind es wirklich über 20 Punkte, muss euer Vorgänger-Team 2 Strafkarten nachziehen und ihr dürft eine neue Runde anfangen.
            </Text>
            <Text style={styles.baseText}>
              Ansonsten müsst ihr selbst 2 Strafkarten ziehen und das Team links von euch fängt an.
              Wenn ihr eure letzte Handkarte abgelegt habt, ohne dass das Team nach euch aufdeckt, bekommt ihr einen Spiel-Punkt und zieht 5 neue Karten nach.
              Das erste Team mit 5 Spiel-Punkten gewinnt!
            </Text>

            <Text style={styles.baseText}>
            Das folgende Video erklärt euch nochmal im Detail, wie man das One Planet Game
            spielt. Danach könnt ihr euch durch eine beispielhafte, interaktive Spielrunde
            klicken, um ein Gefühl für den Spielablauf zu bekommen.
            </Text>

            <View style={styles.ytVideo}>
              <WebView
                style={styles.webViewStyle}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{uri: 'https://www.youtube.com/embed/VOMshbgY1oc' }}
              />
            </View>

          </View>

          <Card onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.setState({
                card1y: layout.y
              })
          }}>
            <View style={styles.cardColumn}>
              <View style={styles.cardRow}>
                <View style={styles.cardContent}>
                  <Card.Title
                    title='Team A'
                    titleStyle={styles.cardTitle}
                    subtitleStyle={styles.cardSubtitle}/>
                  <Card.Content>
                    <Title style={styles.contentTitle}>Team A beginnt das Spiel und legt die erste Karte.</Title>
                  </Card.Content>
                </View>
                <View style={styles.imageView}>
                  <Lightbox activeProps={{style: styles.lightbox}}>
                    <Image
                      source={
                        require('../assets/images/karte1.png')
                      }
                      style={styles.smallCards}
                    />
                  </Lightbox>
                <Card.Content>
                  <Title style={styles.zoomTitle}>(Klicke zum Zoomen)</Title>
                </Card.Content>
                </View>
              </View>
              <View style={styles.buttonWrappers}>
                  <TouchableOpacity style={styles.buttonWrappers}
                    onPress={() => {
                      this.setState({
                        card1disable: true,
                        card2: true
                      });
                    }}>
                    <Button disabled={this.state.card1disable}
                    style={styles.nomarginButtons}
                    labelStyle={styles.buttonsLabelStyle}
                    mode="contained"> Weiter </Button>
                  </TouchableOpacity>
                </View>
            </View>
          </Card>

          {this.state.card2 == true ? 
          <Card style={styles.lowerCards}
            onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.scrollToPosition(layout.y);
              this.setState({
                card2y: layout.y
              })
          }}>
            <View style={styles.cardColumn}>
              <View style={styles.cardRow}>
                <View style={styles.cardContent}>
                  <Card.Title
                    title='Team B'
                    titleStyle={styles.cardTitle}
                    subtitleStyle={styles.cardSubtitle}/>
                  <Card.Content>
                    <Title style={styles.contentTitle}>Team B glaubt, dass die erste Karte noch nicht die 20 Punkte übersteigt und spielt eine weitere Karte.</Title>
                  </Card.Content>
                </View>
                <View style={styles.imageView}>
                  <Lightbox activeProps={{style: styles.lightbox}}>
                    <Image
                      source={
                        require('../assets/images/karte2.png')
                      }
                      style={styles.smallCards}
                    />
                  </Lightbox>
                <Card.Content>
                    <Title style={styles.zoomTitle}>(Klicke zum Zoomen)</Title>
                </Card.Content>
                </View>
              </View>
              <View style={styles.buttonWrappers}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        card1disable: false,
                        card2: false
                      });
                      setTimeout(() => {
                        this.scrollToPosition(this.state.card1y);
                      });
                    }}>
                    <Button disabled={this.state.card2disable}
                    style={styles.nomarginButtons}
                    labelStyle={styles.buttonsLabelStyle}
                    mode="contained"> Zurück </Button>
                  </TouchableOpacity>
                  <TouchableOpacity
                  onPress={() => { 
                      this.setState({
                        card2disable: true,
                        card3: true
                      });
                    }}>
                    <Button disabled={this.state.card2disable}
                    style={styles.buttons}
                    labelStyle={styles.buttonsLabelStyle}
                    mode="contained"> Weiter </Button>
                  </TouchableOpacity>
              </View>
            </View>
          </Card>
          : null}

          {this.state.card3 == true ? 
          <Card style={styles.lowerCards}
            onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.scrollToPosition(layout.y);
              this.setState({
                card3y: layout.y
              })
            }}>
            <View style={styles.thirdCardContent}>
              <Card.Title
                  title='Team C'
                  titleStyle={styles.cardTitle}
                  subtitleStyle={styles.cardSubtitle}/>
                <Card.Content>
                  <Title style={styles.contentTitle}>Team C ist sich aber nun sicher, dass die beiden ersten Karten mehr als 20 Punkte ausmachen und deckt auf.</Title>
                </Card.Content>
                <View style={styles.buttonWrappers}>
                  <TouchableOpacity onPress={() => {
                    this.setState({
                      card2disable: false,
                      card3: false
                    });
                    setTimeout(() => {
                      this.scrollToPosition(this.state.card2y);
                    });
                  }}>
                  <Button disabled={this.state.card3disable}
                   style={styles.nomarginButtons}
                   labelStyle={styles.buttonsLabelStyle}
                   mode="contained" > Zurück </Button>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => { 
                      this.setState({
                        card3disable: true,
                        card4: true
                      });
                    }}>
                    <Button disabled={this.state.card3disable}
                    style={styles.buttons}
                    labelStyle={styles.buttonsLabelStyle}
                    mode="contained"> Weiter </Button>
                  </TouchableOpacity>
              </View>
            </View>
          </Card>
          : null}

          {this.state.card4 == true ? 
          <Card style={styles.lowerCards}
          onLayout={event => {
            const layout = event.nativeEvent.layout;
            this.scrollToPosition(layout.y);

          }}>
            <View style={styles.lastView}>
              <View>
              <Card.Title
                  title='Ergebnis'
                  subtitle='Karten werden aufgedeckt'
                  titleStyle={styles.cardTitle}
                  subtitleStyle={styles.cardSubtitle}/>
              </View>
              <View style={styles.lastCardContent}>
                <View>
                  <Lightbox activeProps={{style: styles.lightbox}}>
                    <Image
                      source={
                        require('../assets/images/karte1_back.png')
                      }
                      style={styles.smallCards}
                      />
                  </Lightbox>
                  <Card.Content>
                    <Title style={styles.zoomTitle}>(Klicke zum Zoomen)</Title>
                  </Card.Content>
                </View>
                <View>
                  <Lightbox activeProps={{style: styles.lightbox}}>
                    <Image
                      source={
                        require('../assets/images/karte2_back.png')
                      }
                      style={styles.smallCards}
                    />
                  </Lightbox>
                  <Card.Content>
                    <Title style={styles.zoomTitle}>(Klicke zum Zoomen)</Title>
                  </Card.Content>
                </View>
              </View>
              
              <View style={styles.resultView}>
               <Card.Content>
                  <Title style={styles.contentTitle}>Ergebnis: 43</Title>
                  <Title style={styles.contentTitle}>Team C hat Recht! Die Winterjacke (3) und das Fastfood (40) ergeben zusammen deutlich mehr als 20 Punkte. 
                  Somit muss Team B 2 Strafkarten ziehen, wobei Team C die nächste Runde mit einer neuen Karte beginnen darf.
                  Falls Team C in dieser Runde außerdem alle Karten losgeworden ist, hat es diese Runde gewonnen und erhält einen Punkt.</Title>
                </Card.Content>
                <View style={styles.buttonWrappers}>
                  <TouchableOpacity onPress={() => {
                    this.setState({
                      card3disable: false,
                      card4: false
                    });
                    setTimeout(() => {
                      this.scrollToPosition(this.state.card3y);
                    });
                  }}>
                    <Button style={styles.nomarginButtons}
                    labelStyle={styles.buttonsLabelStyle}
                      mode="contained"> Zurück </Button>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {
                      this.setState({
                        card1disable: false,
                        card2: false,
                        card2disable: false,
                        card3: false,
                        card3disable: false,
                        card4: false,
                        card4disable: false
                      });
                      this.scrollToPosition(this.state.card1y);
                    }}>
                    <Button style={styles.buttons}
                     labelStyle={styles.buttonsLabelStyle}
                     mode="contained"> Neustart </Button>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Card>
          : null}
        </View>
      </ScrollView>
    );
    } else {
      return(<ActivityIndicator size="small"/>)
    }
  }
}

  const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: "#ffffff"
    },
    wrappingView: {
      padding: 10,
      width: "100%",
      height: "100%",
      flexDirection: 'column',
      justifyContent: 'space-between',
      backgroundColor: "#ffffff"
    },
    introductionView: {
      padding: 15,
      paddingTop: 20,
      width: "100%",
      height: "100%",
      flex: 1,
      flexDirection:"column",
      justifyContent: 'center',
      alignItems: 'flex-start'
    },
    horizontalLine: {
      borderBottomColor: 'black',
      borderBottomWidth: 2,
      margin: 5
    },
    titleText: {
      fontFamily: 'Lato-Bold',
      fontSize: 20,
    },
    baseText: {
      fontFamily: 'Lato-Regular',
      fontSize: 15,
      paddingTop: 10
    },
    webViewStyle: {
      margin: 10,
      opacity: 0.99
    },
    ytVideo: {
      width: '100%', 
      height: 230
    },
    cardColumn: {
      flexDirection: 'column',
      justifyContent: 'space-between',
      padding: 10
    },
    cardRow: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    cardContent: {
      width: "50%",
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: "stretch",
      padding: 10
    },
    thirdCardContent: {
      width: "100%",
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: "stretch",
      padding: 10
    },
    cardTitle: {
      fontFamily: 'Lato-Bold',
      fontSize: 30
    },
    cardSubtitle: {
      fontFamily: 'Lato-Regular',
      fontSize: 15
    },
    contentTitle: {
      fontFamily: 'Lato-Regular',
      fontSize: 20,
      marginBottom: 10
    },
    smallCards: {
      flex: 1,
      resizeMode: 'contain',
      width: '100%',
      height: 250,
    },
    playerTitle: {
      fontFamily: "Lato-Bold",
      fontSize: 25,
      paddingLeft: 8
    },
    playerText: {
      fontFamily: "Lato-Regular",
      fontSize: 18,
      paddingLeft: 8
    },
    buttonWrappers: {
      width: '100%',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: "stretch",
    },
    buttons: {
      backgroundColor: '#acce37',
      marginTop: 10
    },
    nomarginButtons: {
      backgroundColor: '#acce37'
    },
    nomarginButtonsLabel: {
      fontFamily: 'Lato-Bold'
    },
    imageView: {
      padding: 10
    },
    lightbox: {
      resizeMode: 'contain',
      flex: 1,
      width: null
    },
    zoomTitle: {
      fontFamily: 'Lato-Regular',
      fontSize: 10,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: "center"
    },
    lowerCards: {
      marginTop: 10,
      width: '100%',
    },
    lastView: {
      width: '100%',
      flexDirection: 'column',
      justifyContent: 'space-around',
      padding: 10
    },
    lastCardContent: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    resultView: {
      width: "100%",
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: "stretch"
    }
  })