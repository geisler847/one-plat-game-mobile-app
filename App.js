import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { LogBox } from 'react-native';


// Screens
import HomeScreen from './components/HomeScreen';
import QuizScreen from './components/QuizScreen';
import GameManualScreen from './components/GameManualScreen';
import CardsScreen from './components/CardsScreen';
import FootprintScreen from './components/FootprintScreen';
import FaqScreen from './components/FaqScreen';
import AboutScreen from './components/AboutScreen';
import EnergyCardsScreen from './components/categories/EnergyCardsScreen';
import FreetimeCardsScreen from './components/categories/FreetimeCardsScreen';
import NutritionCardsScreen from './components/categories/NutritionCardsScreen';
import ClothesCardsScreen from './components/categories/ClothesCardsScreen';
import ConsumeCardsScreen from './components/categories/ConsumeCardsScreen';
import HouseholdCardsScreen from './components/categories/HouseholdCardsScreen';
import TransportCardsScreen from './components/categories/TransportCardsScreen';


LogBox.ignoreLogs(['Remote debugger']);

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Quiz: QuizScreen,
    Spielanleitung: GameManualScreen,
    Kategorien: CardsScreen,
    Energie: EnergyCardsScreen,
    Freizeit: FreetimeCardsScreen,
    Ernährung: NutritionCardsScreen,
    Kleidung: ClothesCardsScreen,
    Konsum: ConsumeCardsScreen,
    Haushalt: HouseholdCardsScreen,
    Transport: TransportCardsScreen
  },
  {
    initialRouteName: 'Home',
    headerMode: 'screen'
  },
  {
    navigationOptions: ({
      headerShown: false
    })
  }
);


const TabNavigator = createMaterialBottomTabNavigator({
  Home: AppNavigator,
  FAQ: FaqScreen,
  Footprint: FootprintScreen,
  About: AboutScreen
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Home') {
        iconName = 'md-globe'
      } else if (routeName === 'Footprint') {
        iconName = 'leaf';
      } else if (routeName === 'FAQ') {
        iconName = 'frequently-asked-questions';
      } else if (routeName === 'About') {
        iconName = 'md-information-circle-outline';
      }
      if (iconName === 'leaf' || iconName === 'frequently-asked-questions') {
        return <MaterialCommIcons name={iconName} size={25} color={tintColor} />;
      }
      return <Ionicons name={iconName} size={25} color={tintColor} />;
    },
    activeColor: 'white',  
    inactiveColor: 'black',  
    barStyle: { backgroundColor: '#acce37' },
    shifting: false
  }
  ),
}, 
{  
  initialRouteName: "Home",  
  activeTintColor: 'white',  
  inactiveTintColor: 'black',  
},
);

const AppContainer = createAppContainer(TabNavigator);

export default class App extends React.Component {

  render() {
    return (
        <AppContainer />
    );
  }
}